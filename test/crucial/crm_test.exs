defmodule Crucial.CRMTest do
  use Crucial.DataCase

  alias Crucial.CRM

  describe "leads" do
    alias Crucial.CRM.Lead

    import Crucial.CRMFixtures

    @invalid_attrs %{company: nil, description: nil, email: nil, name: nil, phone: nil}

    test "list_leads/0 returns all leads" do
      lead = lead_fixture()
      assert CRM.list_leads() == [lead]
    end

    test "get_lead!/1 returns the lead with given id" do
      lead = lead_fixture()
      assert CRM.get_lead!(lead.id) == lead
    end

    test "create_lead/1 with valid data creates a lead" do
      valid_attrs = %{company: "some company", description: "some description", email: "some email", name: "some name", phone: 42}

      assert {:ok, %Lead{} = lead} = CRM.create_lead(valid_attrs)
      assert lead.company == "some company"
      assert lead.description == "some description"
      assert lead.email == "some email"
      assert lead.name == "some name"
      assert lead.phone == 42
    end

    test "create_lead/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CRM.create_lead(@invalid_attrs)
    end

    test "update_lead/2 with valid data updates the lead" do
      lead = lead_fixture()
      update_attrs = %{company: "some updated company", description: "some updated description", email: "some updated email", name: "some updated name", phone: 43}

      assert {:ok, %Lead{} = lead} = CRM.update_lead(lead, update_attrs)
      assert lead.company == "some updated company"
      assert lead.description == "some updated description"
      assert lead.email == "some updated email"
      assert lead.name == "some updated name"
      assert lead.phone == 43
    end

    test "update_lead/2 with invalid data returns error changeset" do
      lead = lead_fixture()
      assert {:error, %Ecto.Changeset{}} = CRM.update_lead(lead, @invalid_attrs)
      assert lead == CRM.get_lead!(lead.id)
    end

    test "delete_lead/1 deletes the lead" do
      lead = lead_fixture()
      assert {:ok, %Lead{}} = CRM.delete_lead(lead)
      assert_raise Ecto.NoResultsError, fn -> CRM.get_lead!(lead.id) end
    end

    test "change_lead/1 returns a lead changeset" do
      lead = lead_fixture()
      assert %Ecto.Changeset{} = CRM.change_lead(lead)
    end
  end
end
