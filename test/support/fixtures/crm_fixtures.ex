defmodule Crucial.CRMFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Crucial.CRM` context.
  """

  @doc """
  Generate a lead.
  """
  def lead_fixture(attrs \\ %{}) do
    {:ok, lead} =
      attrs
      |> Enum.into(%{
        company: "some company",
        description: "some description",
        email: "some email",
        name: "some name",
        phone: 42
      })
      |> Crucial.CRM.create_lead()

    lead
  end
end
