defmodule CrucialWeb.LeadLiveTest do
  use CrucialWeb.ConnCase

  import Phoenix.LiveViewTest
  import Crucial.CRMFixtures

  @create_attrs %{company: "some company", description: "some description", email: "some email", name: "some name", phone: 42}
  @update_attrs %{company: "some updated company", description: "some updated description", email: "some updated email", name: "some updated name", phone: 43}
  @invalid_attrs %{company: nil, description: nil, email: nil, name: nil, phone: nil}

  defp create_lead(_) do
    lead = lead_fixture()
    %{lead: lead}
  end

  describe "Index" do
    setup [:create_lead]

    test "lists all leads", %{conn: conn, lead: lead} do
      {:ok, _index_live, html} = live(conn, ~p"/leads")

      assert html =~ "Listing Leads"
      assert html =~ lead.company
    end

    test "saves new lead", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/leads")

      assert index_live |> element("a", "New Lead") |> render_click() =~
               "New Lead"

      assert_patch(index_live, ~p"/leads/new")

      assert index_live
             |> form("#lead-form", lead: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#lead-form", lead: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/leads")

      html = render(index_live)
      assert html =~ "Lead created successfully"
      assert html =~ "some company"
    end

    test "updates lead in listing", %{conn: conn, lead: lead} do
      {:ok, index_live, _html} = live(conn, ~p"/leads")

      assert index_live |> element("#leads-#{lead.id} a", "Edit") |> render_click() =~
               "Edit Lead"

      assert_patch(index_live, ~p"/leads/#{lead}/edit")

      assert index_live
             |> form("#lead-form", lead: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#lead-form", lead: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/leads")

      html = render(index_live)
      assert html =~ "Lead updated successfully"
      assert html =~ "some updated company"
    end

    test "deletes lead in listing", %{conn: conn, lead: lead} do
      {:ok, index_live, _html} = live(conn, ~p"/leads")

      assert index_live |> element("#leads-#{lead.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#leads-#{lead.id}")
    end
  end

  describe "Show" do
    setup [:create_lead]

    test "displays lead", %{conn: conn, lead: lead} do
      {:ok, _show_live, html} = live(conn, ~p"/leads/#{lead}")

      assert html =~ "Show Lead"
      assert html =~ lead.company
    end

    test "updates lead within modal", %{conn: conn, lead: lead} do
      {:ok, show_live, _html} = live(conn, ~p"/leads/#{lead}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Lead"

      assert_patch(show_live, ~p"/leads/#{lead}/show/edit")

      assert show_live
             |> form("#lead-form", lead: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#lead-form", lead: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/leads/#{lead}")

      html = render(show_live)
      assert html =~ "Lead updated successfully"
      assert html =~ "some updated company"
    end
  end
end
