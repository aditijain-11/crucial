defmodule Crucial.Page do
  alias Crucial.Page.Content

  use NimblePublisher,
    build: Content,
    from: Application.app_dir(:crucial, "priv/pages/*.md"),
    as: :pages,
    highlighters: [:makeup_elixir, :makeup_erlang]

  # And finally export them
  def all_pages, do: @pages

  defmodule NotFoundError, do: defexception [:message, plug_status: 404]

  def get_page_by_id!(id) do
    Enum.find(all_pages(), &(&1.id == to_string(id))) ||
      raise NotFoundError, "page with id=#{id} not found"
  end
end
