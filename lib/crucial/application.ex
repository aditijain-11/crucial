defmodule Crucial.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      CrucialWeb.Telemetry,
      # Start the Ecto repository
      Crucial.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: Crucial.PubSub},
      # Start Finch
      {Finch, name: Crucial.Finch},
      # Start the Endpoint (http/https)
      CrucialWeb.Endpoint
      # Start a worker by calling: Crucial.Worker.start_link(arg)
      # {Crucial.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Crucial.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    CrucialWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
