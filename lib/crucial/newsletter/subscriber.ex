defmodule Crucial.Newsletter.Subscriber do
  use Ecto.Schema
  import Ecto.Changeset

  schema "subscriber" do
    field :email, :string
    field :is_active, :boolean, default: false
    field :is_compromized, :boolean, default: false
    field :is_verified, :boolean, default: false
    field :name, :string, default: ""

    timestamps()
  end

  @doc false
  def changeset(subscriber, attrs) do
    subscriber
    |> cast(attrs, [:name, :email, :is_active, :is_compromized, :is_verified])
    |> validate_required([:email, :is_active, :is_compromized, :is_verified])
  end
end
