defmodule Crucial.CRM.Lead do
  use Ecto.Schema
  import Ecto.Changeset

  schema "leads" do
    field :company, :string
    field :description, :string
    field :email, :string
    field :name, :string
    field :phone, :string

    timestamps()
  end

  @doc false
  def changeset(lead, attrs) do
    lead
    |> cast(attrs, [:name, :phone, :email, :company, :description])
    |> validate_required([:name, :phone, :email, :company, :description])
  end
end
