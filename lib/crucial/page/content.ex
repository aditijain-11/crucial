defmodule Crucial.Page.Content do
  @enforce_keys [:id, :title, :body]
  defstruct [:id, :title, :body]

  def build(filename, attrs, body) do
    [id] = filename |> Path.rootname() |> Path.split() |> Enum.take(-1)
    struct!(__MODULE__, [id: filename, body: body, id: id ] ++ Map.to_list(attrs))
  end
end
