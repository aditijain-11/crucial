defmodule Crucial.Repo do
  use Ecto.Repo,
    otp_app: :crucial,
    adapter: Ecto.Adapters.Postgres
end
