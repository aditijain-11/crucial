defmodule Crucial.NotificationMail do
  import Swoosh.Email

  def subscribe(subscriber) do
    default_sender_name=Application.get_env(:crucial, Crucial.Mailer)[:default_sender_name]
    default_sender_email=Application.get_env(:crucial, Crucial.Mailer)[:default_sender_email]
    new()
    |> to({"", subscriber.email})
    |> from({default_sender_name, default_sender_email})
    |> subject("Thanks for subscribing.")
    |> html_body("Thanks for subscribing #{subscriber.email}")
    |> text_body("Thanks for subscribing #{subscriber.email}")
  end

  def notify_new_lead(lead) do
    default_sender_name=Application.get_env(:crucial, Crucial.Mailer)[:default_sender_name]
    default_sender_email=Application.get_env(:crucial, Crucial.Mailer)[:default_sender_email]

    new()
    |> to({"info", "info@essentia.dev"})
    |> from({default_sender_name, default_sender_email})
    |> subject("New Lead | #{lead.name}")
    |> html_body("#{lead.name}-#{lead.email} says <br> #{lead.description}")
    |> text_body("#{lead.name}-#{lead.email} says <br> #{lead.description}")

  end
end
