defmodule CrucialWeb.SaveRequestUri do
  @doc """
  copied from https://github.com/phoenixframework/phoenix_live_view/issues/1389#issuecomment-1496644880
  """

  def on_mount(:save_request_uri, _params, _session, socket),
    do:
  {:cont,
   Phoenix.LiveView.attach_hook(
     socket,
     :save_request_path,
     :handle_params,
     &save_request_path/3
   )}

  defp save_request_path(_params, url, socket) do
    {:cont, Phoenix.Component.assign(socket, :current_uri, URI.parse(url) |> Map.get(:path))}
  end

end
