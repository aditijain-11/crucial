defmodule CrucialWeb.PagesLive do
  use CrucialWeb, :live_view

  # alias Crucial.Page
  # %{assigns: %{live_action: :taxonomy_tree}} = socket
  def mount(_params, _session, %{assigns: %{live_action: live_action}} = socket) do
    content = Crucial.Page.get_page_by_id!(live_action)
    {:ok,
     socket
     |> assign(:page_title, content.title)
     |> assign(:content, content)
    }
  end

end
