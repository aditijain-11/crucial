
defmodule CrucialWeb.HomePageLive do
  use CrucialWeb, :live_view

  @client_logos [
    "analytics-vidhya.svg",
    "astra-techz.svg",
    "blubyn.svg",
    # "brooklyn-hlth.svg",
    "careers-360.svg",
    # "episource.svg",
    "go-andamans.svg",
    "iit-bombay.svg",
    "jsm.svg",
    "life-force-fitness.svg",
    # "optum.svg",
    "spendcubes.svg",
  ]

  @services [
    %{
      name: "Design",
      description: "Create user-friendly and intuitive software solutions that deliver an exceptional user experience."
    },
    %{
      name: "Development",
      description: "Build custom software solutions using cutting-edge technology and best practices."
    },
    %{
      name: "DevOps",
      description: "Streamline software development processes with automation and continuous integration and delivery."
    },
    %{
      name: "Data",
      description: "Transform data into strategic advantage with our expertise in data engineering, machine learning, and AI."
    },

  ]

  @testimonials [
    %{
      heading: "Impressed ... ",
      content: " I was impressed by the long term thinking of Essentia folks and their ability to create solutions which are reusable and generic in nature which helped us scale from 30K odd users to 250K users with ease.",
      author: "Anand Mishra",
      designation: "CTO at Analytics Vidhya",
      image: "anand-mishra.jpg"
    },
    %{
      heading: "Pleasure working with Essentia",
      content: "Essentia Softserv's professionalism and technical capabilities were extremely impressive. They were quick to understand our complex system and it was a pleasure working with them.",
      author: "Arastu Zakia",
      designation: "Chief Product Officer at Careers360",
      image: "arastu-zakia.jpg"
    },
    %{
      heading: "Technical debt avoided",
      content: "Whenever we got ourselves in a tangle we would video link. Consequently we were able to evolve with the security of knowing that we were using industry best practices, and avoiding incurring technical debt.",
      author: "Zorawar Purohit",
      designation: "Director at AstraTechz",
      image: "zorawar-purohit.jpg"
    },
    %{
      heading: "Brilliant ...",
      content: "Excellent team, fabulous timely delivery. These are brilliant at going beyond the basics and delivering world class scalable technology. I am certain to build more with them ahead.",
      author: "Pi (Sam King)",
      designation: "CTO at CueAudio",
      image: "pi.jpg"
    },
    %{
      heading: "Highly Recommended",
      content: "Anuvrat solved my AWS configuration issue quickly while educating me on the contents of these files. Very impressed with how quickly he jumped in and was able to problem solve. Highly recommend working with him.",
      author: "Jeremiah Parrack",
      designation: "CEO at Jeremiah Parrack LLC",
      image: "jeremiah-parrack.png"
    },
    %{
      heading: "The Essentia team is a bunch of pros.",
      content: "They're not only knowledgeable and skilled, they are intellectually engaged in the problem and want to develop a sophisticated and scalable solution. We've been loving working with them and plan to continue to do so as we grow.",
      author: "Dr. Anzar Abbas, PhD.",
      designation: "Founder of a stealth startup.",
      image: "anzar-abbas.jpg"
    },
    %{
      heading: "Look no further!",
      content: "Clients, look no further! Essentia is a highly professional, communicative, and efficient team. They delivered high-quality software on time and on budget, and I highly recommend their services.",
      author: "Vijay Yadav",
      designation: "CTO @ Stealth",
      image: "vijay-yadav.jpg"
    },
    %{
      heading: "Impressive team skill!",
      content: "    We've been with Essentia for over 10 years. They have an impressive team skilled in modern dev languages, are able to onboard and bring value quickly, and communicate effectively to deliver high quality products.",
      author: "Dr. Thomas Barus",
      designation: "Barus Consulting",
      image: "thomas-barus.jpg"
    },



  ]
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(:client_logos, @client_logos)
     |> assign(:services, @services)
     |> assign(:testimonials, @testimonials)
     |> assign(:page_title, "Home")
    }
  end

  attr :client_logos, :list, default: []

  def clientele(assigns) do
    ~H"""
    <div class="grid grid-cols-2 gap-8 text-gray-500 sm:gap-12 md:grid-cols-3 lg:grid-cols-6 dark:text-gray-400">
    <%= for logo <- @client_logos do %>
    <div class="flex justify-center items-center">
    <img class="h-9 hover:text-gray-900 dark:hover:text-white" src={"/images/client-logo/#{logo}"}/>
    </div>
    <% end %>
    </div>
    """
  end

  attr :background, :string, default: "bg-white"
  attr :heading, :string, required: :true
  attr :tagline, :string, required: :true
  slot :inner_block, required: :true
  def section(assigns) do
    ~H"""
    <section class={"dark:bg-gray-900 #{@background}"}>
    <div class="py-8 px-4 mx-auto max-w-screen-xl sm:py-16 lg:px-6">
    <div class="mx-auto max-w-screen-md text-center mb-8 lg:mb-16">
    <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white"><%= @heading %></h2>
    <p class="font-light text-gray-500 dark:text-gray-400 sm:text-xl"> <%= @tagline%> </p>
    </div>
    <%= render_slot(@inner_block) %>
    </div>
    </section>
    """
  end

  attr :heading, :string, required: :true
  attr :description, :string, required: :true
  def service_card(assigns) do
    ~H"""
    <div class="p-6 bg-white rounded shadow dark:bg-gray-800">
    <div class="flex justify-center items-center mb-4 w-10 h-10 rounded bg-primary-100 lg:h-12 lg:w-12 dark:bg-primary-900">
    <svg class="w-5 h-5 text-primary-600 lg:w-6 lg:h-6 dark:text-primary-300" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3 3a1 1 0 000 2v8a2 2 0 002 2h2.586l-1.293 1.293a1 1 0 101.414 1.414L10 15.414l2.293 2.293a1 1 0 001.414-1.414L12.414 15H15a2 2 0 002-2V5a1 1 0 100-2H3zm11.707 4.707a1 1 0 00-1.414-1.414L10 9.586 8.707 8.293a1 1 0 00-1.414 0l-2 2a1 1 0 101.414 1.414L8 10.414l1.293 1.293a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path></svg>
    </div>
    <h3 class="mb-2 text-xl font-bold dark:text-white"><%= @heading %></h3>
    <p class="font-light text-gray-500 dark:text-gray-400"><%= @description %></p>
    </div>
    """
  end

  def testimonial_card(assigns) do
    ~H"""
    <figure class="p-6 bg-gray-50 rounded dark:bg-gray-800">
      <blockquote class="text-sm text-gray-500 dark:text-gray-400">
        <h3 class="text-lg font-semibold text-gray-900 dark:text-white"><%= @heading %></h3>
        <p class="my-4">"<%= @content %>”</p>
      </blockquote>
      <figcaption class="flex items-center space-x-3">
        <img class="w-9 h-9 rounded-full" src={"/images/testimonial-pictures/#{@image}"}  alt="profile picture">
        <div class="space-y-0.5 font-medium dark:text-white">
          <div> <%= @author %> </div>
          <div class="text-sm font-light text-gray-500 dark:text-gray-400"> <%= @designation %> </div>
        </div>
      </figcaption>
    </figure>
    """
  end
end
