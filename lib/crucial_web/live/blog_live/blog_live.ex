defmodule CrucialWeb.BlogLive.Index do
  use CrucialWeb, :live_view

  alias Crucial.Blog

  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(:posts, Blog.all_posts())
     |> assign(:page_title, "Blog")
    }
  end

end

defmodule CrucialWeb.BlogLive.Show do
  use CrucialWeb, :live_view

  alias Crucial.Blog


  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    post = Blog.get_post_by_id!(id)
    {:noreply,
     socket
     |> assign(:post, post)
     |> assign(:page_title, post.title)
    }
  end

end
