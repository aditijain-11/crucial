defmodule CrucialWeb.ContactLive do
  use CrucialWeb, :live_view


  alias Crucial.CRM.Lead

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(:lead, %Lead{})
     |> assign(:leads, [])
     |> assign(:page_title, "Contact Form")
     |> assign(:live_action, :new)
    }
  end

  @impl true
  def handle_info({CrucialWeb.LeadLive.FormComponent, {:saved, lead}}, socket) do
    Crucial.NotificationMail.notify_new_lead(lead)
    |> Crucial.Mailer.deliver()
    {:noreply,
     socket
        |> assign(:lead, %Lead{})
        # |> assign(:leads, [])
        # |> assign(:page_title, "another new lead")
        # |> assign(:live_action, :new)
        |> put_flash(:info, "Cheers. We shall soon reach out to you.")
        |> push_redirect(to: "/")
     }
  end

  # defp apply_action(socket, :new, _params) do
  #   socket
  #   |> assign(:page_title, "New Lead")
  #   |> assign(:lead, %Lead{})
  # end

end
