defmodule CrucialWeb.LeadLive.Index do
  use CrucialWeb, :live_view

  alias Crucial.CRM
  alias Crucial.CRM.Lead

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :leads, CRM.list_leads())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Lead")
    |> assign(:lead, CRM.get_lead!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Lead")
    |> assign(:lead, %Lead{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Leads")
    |> assign(:lead, nil)
  end

  @impl true
  def handle_info({CrucialWeb.LeadLive.FormComponent, {:saved, lead}}, socket) do
    {:noreply, stream_insert(socket, :leads, lead)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    lead = CRM.get_lead!(id)
    {:ok, _} = CRM.delete_lead(lead)

    {:noreply, stream_delete(socket, :leads, lead)}
  end
end
