defmodule CrucialWeb.LeadLive.FormComponent do
  use CrucialWeb, :live_component

  alias Crucial.CRM

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Share your deatils and we will reach out to you.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="lead-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:name]} type="text" label="Name" />
        <.input field={@form[:phone]} type="tel" label="Phone" />
        <.input field={@form[:email]} type="email" label="Email" />
        <.input field={@form[:company]} type="text" label="Company" />
        <.input field={@form[:description]} type="textarea" label="Description" />
        <:actions>
          <.button phx-disable-with="Saving...">Submit</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{lead: lead} = assigns, socket) do
    changeset = CRM.change_lead(lead)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"lead" => lead_params}, socket) do
    changeset =
      socket.assigns.lead
      |> CRM.change_lead(lead_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"lead" => lead_params}, socket) do
    save_lead(socket, socket.assigns.action, lead_params)
  end

  defp save_lead(socket, :edit, lead_params) do
    case CRM.update_lead(socket.assigns.lead, lead_params) do
      {:ok, lead} ->
        notify_parent({:saved, lead})

        {:noreply,
         socket
         |> put_flash(:info, "Lead updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_lead(socket, :new, lead_params) do
    case CRM.create_lead(lead_params) do
      {:ok, lead} ->
        notify_parent({:saved, lead})

        {
          :noreply,
          socket
          |> put_flash(:info, "Saved. Thanks. We will get in touch with you.")
          # |> push_redirect(to: "/")

          # |> push_patch(to: socket.assigns.patch)
        }

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
