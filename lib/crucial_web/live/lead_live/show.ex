defmodule CrucialWeb.LeadLive.Show do
  use CrucialWeb, :live_view

  alias Crucial.CRM

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:lead, CRM.get_lead!(id))}
  end

  defp page_title(:show), do: "Show Lead"
  defp page_title(:edit), do: "Edit Lead"
end
