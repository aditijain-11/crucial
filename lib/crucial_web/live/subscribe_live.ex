defmodule CrucialWeb.SubscribeLiveComponent do
  use CrucialWeb, :live_component

  alias Crucial.Newsletter.Subscriber
  alias Crucial.Repo

  def render(assigns) do
    ~H"""
    <div>
      <%= if not @new do %>
        <button
          phx-click="reset"
          phx-target={@myself}
          class={[
            "block p-3 pl-10 w-full text-sm text-gray-900 bg-gray-50 ",
            "rounded-l-lg border border-gray-300 focus:ring-primary-500",
            "focus:border-primary-500 dark:bg-gray-700 dark:border-gray-600",
            "dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 ",
            "dark:focus:border-primary-500"
          ]}
        >
          Subscribed. Click to Reset.
        </button>
      <% else %>
        <.form
          class="flex w-full max-w-sm lg:ml-auto"
          for={@form}
          phx-target={@myself}
          phx-change="validate"
          phx-submit="save"
        >
          <div class="relative w-full">
            <label
              for="email"
              class="hidden mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              Email address
            </label>
            <div class="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
              <svg
                class="w-5 h-5 text-gray-500 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z">
                </path>
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
              </svg>
            </div>

            <.local_input field={@form[:email]} placeholder="Your email" />
          </div>
          <button
            type="submit"
            class="py-3 px-5 text-sm text-center text-white rounded-r-lg border cursor-pointer bg-primary-600 border-primary-600 hover:bg-primary-700 focus:ring-4 focus:ring-primary-300 dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800"
          >
            Subscribe
          </button>
        </.form>
      <% end %>
    </div>
    """
  end

  attr :field, Phoenix.HTML.FormField
  attr :rest, :global, include: ~w(disabled form name value placeholder)

  def local_input(assigns) do
    ~H"""
    <input
      id={@field.id}
      name={@field.name}
      value={@field.value}
      class={[
        "block p-3 pl-10 w-full text-sm text-gray-900 bg-gray-50 ",
        "rounded-l-lg border border-gray-300 focus:ring-primary-500",
        "focus:border-primary-500 dark:bg-gray-700 dark:border-gray-600",
        "dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 ",
        "dark:focus:border-primary-500"
      ]}
      {@rest}
    />
    """
  end

  def mount(socket) do
    {:ok, reinitialize(socket)}
  end

  defp reinitialize(socket) do
    subscriber = %Subscriber{}
    changeset = Subscriber.changeset(subscriber, %{})

    socket
    |> assign(:new, true)
    |> assign(:subscriber, subscriber)
    |> assign(:form, to_form(changeset))
  end

  def handle_event("validate", %{"subscriber" => subscriber_params}, socket) do
    subscriber = socket.assigns.subscriber || %Subscriber{}
    changeset = Subscriber.changeset(subscriber, subscriber_params)
    {:noreply, assign(socket, form: to_form(changeset))}
  end

  def handle_event("reset", _params, socket) do
    {:noreply, reinitialize(socket)}
  end

  def handle_event("save", %{"subscriber" => subscriber_params}, socket) do
    subscriber = socket.assigns.subscriber || %Subscriber{}
    changeset = Subscriber.changeset(subscriber, subscriber_params)

    case Repo.insert(changeset) do
      {:ok, subscriber} ->
        Crucial.NotificationMail.subscribe(subscriber) |> Crucial.Mailer.deliver()
        {:noreply,
         socket
         |> assign(:new, false)
         |> put_flash(:info, "Subscription Successful, please verify your email")}

      {:error, changeset} ->
        IO.inspect(changeset)
        {:noreply, assign(socket, form: to_form(changeset))}
    end
  end
end
