defmodule CrucialWeb.Layouts do
  use CrucialWeb, :html

  embed_templates "layouts/*"
end
