defmodule CrucialWeb.PageHTML do
  use CrucialWeb, :html

  embed_templates "page_html/*"
end
