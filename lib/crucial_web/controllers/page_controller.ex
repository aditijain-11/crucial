defmodule CrucialWeb.PageController do
  use CrucialWeb, :controller

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.
    render(conn, :home)
  end

  def careers(conn, _params) do
    render(conn, :careers)
  end

  def privacy(conn, _params) do
    render(conn, :privacy)
  end

  
end
