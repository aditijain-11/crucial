defmodule CrucialWeb.Router do
  use CrucialWeb, :router

  import CrucialWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {CrucialWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CrucialWeb do
    
    pipe_through :browser
    live_session :default,
      on_mount: [
        {CrucialWeb.SaveRequestUri, :save_request_uri}
      ] do

      live "/", HomePageLive
      live "/careers", PagesLive, :careers
      live "/about", PagesLive, :about
      live "/privacy-policy", PagesLive, :privacy
      live "/services", PagesLive, :services
      live "/team", PagesLive, :team

      live "/contact", ContactLive
      live "/blog/", BlogLive.Index
      live "/blog/:id", BlogLive.Show

      live "/case-studies/", CaseStudiesLive.Index
      live "/case-studies/:id", CaseStudiesLive.Show
    end

      # live "/blog"
      #live ""

      # get "/blog", BlogController, :index
      # get "/blog/:id", BlogController, :show

  end

  # Other scopes may use custom stacks.
  # scope "/api", CrucialWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:crucial, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: CrucialWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", CrucialWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :redirect_if_user_is_authenticated,
      on_mount: [
        {CrucialWeb.UserAuth, :redirect_if_user_is_authenticated},
        {CrucialWeb.SaveRequestUri, :save_request_uri}
      ] do
      live "/users/register", UserRegistrationLive, :new
      live "/users/log_in", UserLoginLive, :new
      live "/users/reset_password", UserForgotPasswordLive, :new
      live "/users/reset_password/:token", UserResetPasswordLive, :edit
    end

    post "/users/log_in", UserSessionController, :create
  end

  scope "/", CrucialWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :require_authenticated_user,
      on_mount: [
        {CrucialWeb.UserAuth, :ensure_authenticated},
        {CrucialWeb.SaveRequestUri, :save_request_uri}
      ] do
      live "/users/settings", UserSettingsLive, :edit
      live "/users/settings/confirm_email/:token", UserSettingsLive, :confirm_email

      live "/leads", LeadLive.Index, :index
      live "/leads/new", LeadLive.Index, :new
      live "/leads/:id/edit", LeadLive.Index, :edit

      live "/leads/:id", LeadLive.Show, :show
      live "/leads/:id/show/edit", LeadLive.Show, :edit
    end
  end

  scope "/", CrucialWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete

    live_session :current_user,
      on_mount: [{CrucialWeb.UserAuth, :mount_current_user}] do
      live "/users/confirm/:token", UserConfirmationLive, :edit
      live "/users/confirm", UserConfirmationInstructionsLive, :new
    end
  end
end
