defmodule Crucial.Repo.Migrations.CreateSubscriber do
  use Ecto.Migration

  def change do
    create table(:subscriber) do
      add :name, :string, default: ""
      add :email, :string
      add :is_active, :boolean, default: false, null: false
      add :is_compromized, :boolean, default: false, null: false
      add :is_verified, :boolean, default: false, null: false

      timestamps()
    end
  end
end
